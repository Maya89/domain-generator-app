import React from 'react';

class WordsCreator extends React.Component{
    constructor(){
        super();
        this.state = {
            list_result : [],
            list_visible : false
        };

        this.create_words_list = this.create_words_list.bind(this);

    }

    creat_word(length)
    {
        let result = '';
        let vowel_Letters = ['a','e','i','o','u'];
        let characters = 'abcdefghijklmnopqrstuvwxyz';
        let charactersLength = characters.length;
        let count = 0;
        for ( let i = 0; i < length; i++ ) {

            let character = characters.charAt(Math.floor(Math.random() * charactersLength));
            if(!vowel_Letters.includes(character))
                count++;
            else
                count = 0;
                
            if(count === 3)
                {
                    debugger
                    character = vowel_Letters[Math.floor(Math.random() * vowel_Letters.length)];
                    count = 0;
                }
            
            result += character;
        }
        return result;
    }

    create_words_list(){
        this.state.list_result = [];
        for(let i = 0; i< 5; i++){
        let word = this.creat_word(5);
        this.state.list_result.push(word);
        }
        for(let i = 0; i< 5; i++){
            let word = this.creat_word(6);
            this.state.list_result.push(word);
        }
        this.setState({list_visible:true});
    }

    

    render(){
        return(
            <div>
                <button onClick={this.create_words_list}>Create words list</button>
                {this.state.list_visible &&
                <ul>
                    {this.state.list_result.map(word => (
                        <li>{word}</li>))}
                </ul>
                }
            </div>
        )
    }
}

export default WordsCreator;