import React from 'react';
import './App.css';
import WordsCreator from './Components/wordsCreator'

function App() {
  return (
    <div className="App">
      <WordsCreator/>
    </div>
  );
}

export default App;
